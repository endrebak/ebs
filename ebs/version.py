from sys import version_info

PY3 = version_info > (3, )

__version__ = "0.0.10"
