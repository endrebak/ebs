import pytest
import pandas as pd

from ebs.read_indata import read_indata
from ebs.imports import StringIO


def test_read_indata_with_header(df_header, expected_result_header):

    actual_result = read_indata(df_header, False)

    print(actual_result, "ACTUAL")
    print(expected_result_header, "EXPECTED")

    assert actual_result.equals(expected_result_header)


DF_STRING_HEADER = ("Gene	Fold	LogCPM\n"
                    "Ipcef1	-2.70987558746701	4.80047582653889\n"
                    "Sema3b	2.00143465979322	3.82969788437155\n"
                    "Lcat	2.11219348292396	3.16122865097382\n")


@pytest.fixture
def df_header():

    return StringIO(DF_STRING_HEADER)

@pytest.fixture
def expected_result_header():

    df = pd.read_table(StringIO(DF_STRING_HEADER), header=0, dtype=str, sep="\s+")

    return df


def test_read_indata_with_no_header(df_no_header, expected_result_no_header):
    actual_result = read_indata(df_no_header, True)

    print(actual_result, "ACTUAL")
    print(expected_result_no_header, "EXPECTED")

    assert actual_result.equals(expected_result_no_header)



DF_STRING_NO_HEADER = (u"Ipcef1	-2.70987558746701	4.80047582653889\n"
                        "Sema3b	2.00143465979322	3.82969788437155\n"
                        "Lcat	2.11219348292396	3.16122865097382")

@pytest.fixture
def df_no_header():
    return StringIO(DF_STRING_NO_HEADER)


@pytest.fixture
def expected_result_no_header():
    return pd.read_table(StringIO(DF_STRING_NO_HEADER), header=None, dtype=str, sep="\s+")


def test_read_indata_with_no_header_col(df_no_index_header,
                                        expected_result_no_index_header):

    actual_result = read_indata(df_no_index_header, False)

    print(actual_result, "ACTUAL")
    print(expected_result_no_index_header, "EXPECTED")

    assert actual_result.equals(expected_result_no_index_header)


DF_STRING_NO_INDEX_HEADER = (u"Fold	LogCPM\n"
                             "Ipcef1	-2.70987558746701	4.80047582653889\n"
                             "Sema3b	2.00143465979322	3.82969788437155\n"
                             "Lcat	2.11219348292396	3.16122865097382")


DF_EXPECTED = (u"index   Fold   LogCPM\n"
                "Ipcef1	-2.70987558746701	4.80047582653889\n"
               "Sema3b	2.00143465979322	3.82969788437155\n"
               "Lcat	2.11219348292396	3.16122865097382")

@pytest.fixture
def df_no_index_header():

    return StringIO(DF_STRING_NO_INDEX_HEADER)

@pytest.fixture
def expected_result_no_index_header():

    df = pd.read_table(StringIO(DF_EXPECTED), header=0, dtype=str, sep="\s+")

    return df
