# ebs

Various utilities I need in more than one piece of software or libraries.

Packaged up and made pip-installable with

`pip install ebs`

Probably nothing to see here, but feel free to ask about whatever on the issues page.
